#include <stdio.h>
#include "ft_select.h"

int tputs_putchar(int c)
{
	write(1, &c, 1);
	return (1);
}

int		ft_reset_term(struct termios term)
{
	if (tcgetattr(0, &term) == -1)
		return (-1);
	term.c_lflag = (ICANON | ECHO);
	if (tcsetattr(0, 0, &term) == -1)
		return (-1);
	tputs(tgetstr("ve", NULL), 1, tputs_putchar);
	tputs(tgetstr("te", NULL), 1, tputs_putchar);
	return (0);
}

void	ft_print_opt(t_opt *opt)
{
	t_opt	*walk;
	walk = opt;
	
	while (walk)
	{
		if (walk->cursor)
			tputs(tgetstr("us", NULL), 1, tputs_putchar);
		if (walk->selected)
			tputs(tgetstr("mr", NULL), 1, tputs_putchar);
		printf("%s\n", walk->content);
		tputs(tgetstr("me", NULL), 1, tputs_putchar);
		
		if (walk->end == 1)
			break ;
		walk = walk->next;
	}

}

void	ft_print_results(t_opt *beg)
{
	while (beg)
	{
		if (beg->selected == 1)
		
		beg = beg->next;
	}
}
int     voir_touche(t_opt *opt)
{
	char     buffer[3];
	t_opt	*beg;
	
	beg = opt;
	opt->cursor = 1;
	while (1)
	{
		ft_print_opt(beg);
		read(0, buffer, 3);
		if (buffer[0] == 27 && buffer[1] == 91 && buffer[2] == 65)
		{
			printf("UP\n");
			opt->cursor = 0;
			opt = opt->prev;
			opt->cursor = 1;
		}
		if (buffer[0] == 27 && buffer[1] == 91 && buffer[2] == 66)
		{
			printf("DOWN\n");
			opt->cursor = 0;
			opt = opt->next;
			opt->cursor = 1;
		}
	//	if (buffer[0] == 27 && buffer[1] == 91 && buffer[2] == 68)
	//	if (buffer[0] == 27 && buffer[1] == 91 && buffer[2] == 67)
		if (buffer[0] == 32)
		{
			opt->selected = opt->selected ? 0 : 1;
			opt->cursor = 0;
			opt->next->cursor = 1;
			tputs(tgetstr("kd", NULL), 0, tputs_putchar);
		}
		if (buffer[0] == 10)
		{
			return (1);
		}
		tputs(tgetstr("vi", NULL), 0, tputs_putchar);
		tputs(tgetstr("cl", NULL), 0, tputs_putchar);
	}

	return (0);
}

t_opt	*ft_optnew(char *content)
{
	t_opt	*nw;

	nw = (t_opt *)malloc(sizeof(*nw));
	if (nw)
	{
		nw->content = content;
		nw->selected = 0;
		nw->cursor = 0;
		nw->next = NULL;
		nw->prev = NULL;
		nw->end = 0;
		return (nw);
	}
	else
		return (NULL);
}

void	ft_optadd(t_opt **list, char *content)
{
	t_opt	*walk;

	walk = *list;
	if (walk)
	{
		while (walk->next)
			walk = walk->next;
		walk->next = ft_optnew(content);
		walk->next->prev = walk;
	}
	else
		*list = ft_optnew(content);
}

t_opt	*ft_create_list(char **av)
{
	int	i;
	t_opt	*opt;
	t_opt	*end;

	i = 1;
	while (av[i])
	{
		ft_optadd(&opt, av[i]);
		i++;
	}
	end = opt;
	while (end->next)
		end = end->next;
	end->end = 1;
	end->next = opt;
	opt->prev = end;
	return (opt);
}

int              main(int ac, char **av, char **env)
{
	char           *name_term;
	struct termios term;
	char		*clearstr;
	t_opt		*opt;

	if ((name_term = getenv("TERM")) == NULL)
		return (-1);
	tgetent(NULL, name_term);
	if (tcgetattr(0, &term) == -1)
		return (-1);
	term.c_lflag &= ~(ICANON);
	term.c_lflag &= ~(ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSANOW, &term) == -1)
		return (-1);
	tputs(tgetstr("cl", NULL), 0, tputs_putchar);
	opt = ft_create_list(av);
	if (voir_touche(opt) == 1)
		ft_reset_term(term);
	return (0);
}
/*
   int main()
   {
   char buf[1024];
   char buf2[30];
   char *ap = buf2;
   char *clearstr, *gotostr, *standstr, *stendstr;
   int 	i;

   i = 0;

   tgetent(buf, getenv("TERM"));

   clearstr = tgetstr("cl", &ap);
   gotostr = tgetstr("cm", &ap);
   standstr = tgetstr("so", &ap);
   stendstr = tgetstr("se", &ap);

   fputs(tgoto(gotostr, 20, 10), stdout);
   printf("%d", 1234556);
//fputs(clearstr, stdout);
i++;
}
*/

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/16 14:14:29 by pguzman           #+#    #+#             */
/*   Updated: 2016/03/17 10:36:05 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <term.h>
#include <fcntl.h>
#include <signal.h>

typedef struct s_opt
{
	char	*content;
	int		selected;
	int		cursor;
	int		end;
	struct s_opt *prev;
	struct s_opt *next;
}				t_opt;
